<?php
/** 
  * @file
  * google_drive.admin.inc
  */
  
  
  
/**
  * Implements google_drive_admin
  *
  */
function google_drive_admin() {
  syslog(LOG_ERR, "gdrive admin form");
  $form = array();
  
  $form['google_drive_oauth'] = array(
    '#title' => t('Google Drive OAuth 2.0 Settings'),
  );
  
  $form['google_drive_oauth']['google_drive_app_id'] = array(
    '#type' => 'textfield',
    '#title' => t('App ID'),
    '#description' => t('Define your application ID obtained from the ' .
      'APIs Console project. APIs console is accessible from ' .
      '<a href="https://code.google.com/apis/console" target="_blank">https://code.google.com/apis/console</a>' .
      '. These values are specific for your application.'),
    '#required' => TRUE,
    '#default_value' => variable_get('google_drive_app_id',1),
  );
  
  $form['google_drive_oauth']['google_drive_client_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Client ID'),
    '#description' => t('Define your OAuth 2.0 configuration from APIs Console ' .
      ' project.  The APIs console is accessible from ' .
      '<a href="https://code.google.com/apis/console" target="_blank">https://code.google.com/apis/console</a>' .
      '. These values are specific for your application.'),
    '#required' => TRUE,
  );
  
  $form['google_drive_oauth']['google_drive_client_secret'] = array(
    '#type'        => 'password_confirm',
    '#title'       => t('Client Secret'),
    '#description' => t('OAuth 2.0 client secret'),
    '#required'    => TRUE,
  );
  
  $form['google_drive_oauth']['google_drive_redirect_url'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Redirect URL'),
    '#description'   => t('Define the call back URL'),
    '#disabled'      => TRUE,
    '#default_value' => 'http://localhost',
  );
  
  // since this is a settings form we need to explicitly
  // tell Drupal where to submit it rather than allowing
  // it to, by default, store it in the variables table
  $form['#submit'][] = 'google_drive_admin_submit';
  
  
  return system_settings_form($form);
}

/**
  * Implments hook_form_submit()
  *
  */
function google_drive_admin_submit($form, &$form_state) {
  syslog(LOG_ERR, "got a form!");
  $form_state = array();
}

/**
  * Implements hook_menu()
  *
  */
function google_drive_menu() {
  $items = array();
  
  $items['admin/config/system/google_drive'] = array(
    'title' => 'Google Drive OAuth Configuration',
    'description' => 'Google Drive OAuth configuration information.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('google_drive_admin'),
    'access arguments' => array('administer google_drive settings'),
    'type' => MENU_NORMAL_ITEM,
  ); 
  
  return $items;
}